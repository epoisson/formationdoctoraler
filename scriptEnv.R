rm(list=ls()) # clear all objects
graphics.off() # clear all plots

dirFile="D:/0Nicolas/stock_2D_3D_10K/stock_2D_3D_10K/data/ENV/"
listFiles=list.files(dirFile,full.names = TRUE)
print(listFiles)

df1y=readxl::read_xlsx(listFiles[4],sheet=2)
df3y=readxl::read_xlsx(listFiles[5],sheet=2)
df5y=readxl::read_xlsx(listFiles[6],sheet=2)

############################################
# extract P50 and range for each param
############################################

#fonction pour recuperer les parametres et leurs etendues
recupParamEnv<-function(df,accroche){
  # conservation des P50 fond (b=bottom<200m de profondeur)
  indP50b=grep(pattern = "^P50_b",x=colnames(df))
  dparam=df[,indP50b]
  # conservation des etendues des parametres 
  indP5b=grep(pattern = "^P05_b",x=colnames(df))
  indP95b=grep(pattern = "^P95_b",x=colnames(df))
  etendue=df[,indP95b]-df[,indP5b]
  colnames(etendue)=gsub(pattern="P95",replacement="Range",colnames(etendue))
  dparam=cbind(dparam,etendue)
  colnames(dparam)=paste(accroche,colnames(dparam),sep="")
  colnames(dparam)=gsub(pattern = "Range_b",replacement = "R",x = colnames(dparam))
  colnames(dparam)=gsub(pattern = "P50_b",replacement = "M",x = colnames(dparam))
  oput<-dparam
}

dparam1=recupParamEnv(df1y,accroche = "1y_")
dparam3=recupParamEnv(df3y,accroche = "3y_")
dparam5=recupParamEnv(df5y,accroche = "5y_")

dparam=cbind(dparam1,dparam3,dparam5)

#############################
# retrait des duplicats (meme individu = meme lieu de mesure des param)
##############################
long=df1y$new_long
lat=df1y$new_lat
dparam2=cbind(long,lat,dparam)
ind_removed=which(duplicated(dparam2))
print(length(ind_removed))
dparam=dparam[-ind_removed,]
print(dim(dparam))
long=long[-ind_removed]
lat=lat[-ind_removed]

##################################
# cartographie d'un parametre pour 1/3/5 ans

library("leaflet")
creeCarte<-function(long,lat,param, mini, maxi){
  df=data.frame(lng=long,lat=lat, x=param)
  print(colnames(df))
  df=na.omit(df)
  m <- leaflet() %>% addTiles()
  color_palette <- colorRampPalette(c("blue", "green", "yellow", "red"))
  breaks <- unique(seq(mini, maxi, length.out = 100))
  df$color <- color_palette(100)[cut(df$x, breaks = breaks)]
  m <- m %>% addCircleMarkers(data = df, radius = 0.1, color = ~color, fill = FALSE)
}

#exemple param P50 de Temperature : M_T / etendue R_T
para="M_T"
indpara=grep(pattern=para,x=colnames(dparam),ignore.case=T)
colnames(dparam)[indpara]
minPara=min(unlist(dparam[,indpara]),na.rm=TRUE)
maxPara=max(unlist(dparam[,indpara]),na.rm=TRUE)
m1 <-creeCarte(long,lat,dparam[,indpara[1]],mini=minPara,maxi=minPara)
m3 <-creeCarte(long,lat,dparam[,indpara[2]],mini=minPara,maxi=minPara)
m5 <-creeCarte(long,lat,dparam[,indpara[3]],mini=minPara,maxi=minPara)
m1
m3
m5

##################################
# Spectral Clustering
###################################
# Points scales -> similarité W (ZP)-> Laplacien L (NJW) -> extrait les valp et les vectp  
# sur les K vectprop on effectue PAM=kmedoids 
recursiveSpectralPAM<-function(data,levelMax=4,minPts=7,min.val=0.5){
  label=rep(0,nrow(data))
  for(niv in 1:levelMax){
    cat("niv=",niv,"\n")
    labelPrec=label
    print(unique(labelPrec))
    for(lab in unique(labelPrec)){
      cat("lab=",lab,"\n")
      ajout=1
      ds=scale(data[label==lab,])
      if(nrow(ds)>minPts){
        sim=sClust::compute.similarity.ZP(ds)
        lap=sClust::compute.laplacian.NJW(sim,verbose = FALSE)
        print(lap$eigen$values[0:5])
        K=sum(lap$eigen$values>0.995)+(sum(lap$eigen$values>min.val)>1)
        cat("K=",K)
        if(K>1){
          res=sClust::spectralPAM(sim,K,flagDiagZero = TRUE,verbose = FALSE)
          ajout=res$cluster
        }
      }
      label[label==lab]=ajout+label[label==lab]*10
      print(label)
    }
  }
  out<-label
}

# pour comprendre sur iris
# essai=recursiveSpectralPAM(scale(iris[,1:4]),levelMax=5)
# table(essai,iris$Species)
# table(essai-essai%%10000,iris$Species)
# table(essai-essai%%1000,iris$Species)
# pdfCluster::adj.rand.index(essai,iris$Species)
# pdfCluster::adj.rand.index(essai-essai%%10000,iris$Species)


cl=recursiveSpectralPAM(scale(dparam),levelMax=5)
cl.num=as.numeric(factor(cl))
plot(long,lat,pch=as.character(cl.num),col=cl.num,ylab="lat",xlab="long",main="spectral clustering")
table(cl.num,df1y$GSA[-ind_removed])


# leaflet du clustering
mescouleurs=c("blue","cyan","red","forestgreen","orange","green","yellow","pink","brown","purple","violet","black","grey")
df=data.frame(long,lat,rlabel=cl,label=cl.num,colabel=mescouleurs[cl.num])
library(leaflet)
m <- leaflet() %>% addTiles() 
m <- m %>% addCircleMarkers(data = df, radius = 0.1, color = ~colabel,fill = TRUE)
m


############################
## retrait des parametres très corrélés
## selon seuil
#################
corrplot::corrplot(cor(dparam),type = "upper",method = "circle",tl.col = "black")
seuil=0.75
removed=NULL
mCor=abs(cor(dparam))
nom=colnames(mCor)
nom
N=nrow(mCor)
N
removed=NULL
for(i in 1:(N-1)){
  print(i)
  print(mCor[i,])
  ind=which(mCor[i,(i+1):N]>seuil)
  print(ind+i)
  removed=unique(c(removed,ind+i))
}
print(colnames(dparam)[removed])
dparam=dparam[,-removed]

cl.un=recursiveSpectralPAM(scale(dparam),levelMax=5)
cl.num=as.numeric(factor(cl.un))

plot(long,lat,pch=as.character(cl.num),col=cl.num,ylab="lat",xlab="long",main="spectral clustering")
table(cl.num,df1y$GSA[-ind_removed])


# leaflet du clustering
mescouleurs=c("blue","cyan","red","forestgreen","orange","green","yellow","pink","brown","purple","violet","black","grey")
df=data.frame(long,lat,rlabel=cl,label=cl.num,colabel=mescouleurs[cl.num])
library("leaflet")
m <- leaflet() %>% addTiles() 
m <- m %>% addCircleMarkers(data = df, radius = 0.1, color = ~colabel,fill = TRUE)
m

pdfCluster::adj.rand.index(cl,cl.un)
x11()
boxplot(dparam$`1y_M_ALK`~cl.un)
summary(dparam)

